/* lavacookie.cpp created 4/2/2013
 * Copyright (C) 2013 Matthew Scheirer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../headers/lavacookie.hpp"

const LavaCookie& LavaCookie::instance() const {
  return theInstance;
}

LavaCookie::LavaCookie() throw(PermissionsException) {
  /* These exceptions are used in case the app doesn't have permissions
   * to write to the users home directory.
   * TODO: implement an error message to resolve these rather than crashing
   *   the app. Also consider using baseDir.canonicalPath, etc, instead of macros -
   *   note they would require a lot of copies to get a char* from a QString */
  if(!lc::baseDir.exists() && !lc::baseDir.mkpath(QString(".")))
    throw PermissionsException(APPDIR);
  if(!lc::configFile.open(QIODevice::ReadWrite))
    throw PermissionsException(CFGNAME);

  bool validConfig = true;
  if(lc::configFile.atEnd())
    validConifg = false;
  else {
    config = QJsonDocument::fromBinaryData(configFile.readAll());
    if(!config.isObject())
      validConifg = false;
  }
  if(!validConifg)
    config = QJsonDocument(configObj);
  else
    configObj = config.object();

  QJsonValue profsRef = configObj["profiles"];
  if(profsRef.isArray()) {
    for(QJsonValue prof : profsRef.toArray()) {
      if(prof.isObject()) {
        // I find it odd C++ requires brace-wrapped try blocks.
        try {
          lc::genProfile(prof.toObject());
        } catch(std::string name)
          std::cerr << "Malformed profile in configuration named : " << name << std::endl;
      }
    }
  } else {

  }


  lc::refreshAddons();
}
