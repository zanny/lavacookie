/* lavacookie.cpp created 3/28/2013
 * Copyright (C) 2013 Matthew Scheirer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../headers/lc.hpp"

void lc::addDataDir(QDir dir) {
  if(dir.exists()) {
    QStringList wads = dir.entryList(dataFilters, QDir::Files|QDir::Readable);
    if(!wads.empty() && !dataDirs.
    for(QString wad : wads) {
      if(data[wad] == QDir::current())
        data[wad] = dir
    }
  }
}

/**
 * Syntactic Layout of a Profile json object:
 * {String "name", Date "lastplayed", String "baseGame", Array[String] "addons"}
 * If any value is null or doesn't exist (except addons) it will try to throw the
 * name of the profile as malformed. If it has no name, it says "not a profile".
 */
Profile lc::genProfile(QJsonObject json) throw(std::string) {
  QJsonValueRef val;
  QString strings[];
  for(const QString key : profProperties) {
    val = json[key];
    if(!val.isString())
      return;
    val.s
  }
}

void lc::refreshAddons() throw(PermissionsException) {
  addonDir = QDir(basePath + '/' + ADDONDIR);
  if(!addonDir.exists() && !baseDir.mkdir(ADDONDIR))
    throw PermissionsException(ADDONDIR);
}
