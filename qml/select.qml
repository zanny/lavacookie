import QtQuick 2.0
import QtMobility.systeminfo 1.2

Rectangle {
  id: window
  width: 360
  height: 480

  ListView {
    id: gamesList
    height: 400
    anchors.right: parent.right
    anchors.left: parent.left
    anchors.top: parent.top

    delegate: Rectangle {

    }
    model: ListModel {
      ListElement {
        name: "Grey"
        colorCode: "grey"
      }

      ListElement {
        name: "Red"
        colorCode: "red"
      }

      ListElement {
        name: "Blue"
        colorCode: "blue"
      }

      ListElement {
        name: "Green"
        colorCode: "green"
      }
    }
  }

  Item {
    id: footer
    anchors.top: gamesList.bottom
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.topMargin: 0

    Rectangle {
      id: newGame
      width: parent.width / 3
      height: 60
      color: "#ffffff"
      anchors.verticalCenter: parent.verticalCenter
      anchors.left: parent.left
      anchors.leftMargin: 10
    }

    Rectangle {
      id: getAddons
      width: parent.width / 3
      height: 60
      color: "#ffffff"
      anchors.verticalCenter: parent.verticalCenter
      anchors.left: newGame.right
      anchors.leftMargin: 10
    }

    Rectangle {
      id: options
      width: parent.width / 3
      height: 60
      color: "#ffffff"
      anchors.verticalCenter: parent.verticalCenter
      anchors.right: parent.right
      anchors.rightMargin: 10
    }
  }
}
