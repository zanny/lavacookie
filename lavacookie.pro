# Copyright (C) 2013 Matthew Scheirer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

lessThan(QT_MAJOR_VERSION, 5): error(Requires qt5 support.)

files.sources = assets
ui.source = qml
DEPLOYMENTFOLDERS = files ui

QMAKE_CXXFLAGS += -Wall -Wextra -std=c++11

PRECOMPILED_HEADER = headers/stable.hpp
HEADERS = headers/stable.hpp \
    headers/profile.hpp \
    headers/lc.hpp \
    headers/lavacookie.hpp \
    headers/lcViewer.hpp
SOURCES = \
    sources/main.cpp \
    sources/profile.cpp \
    sources/lc.cpp \
    sources/lavacookie.cpp \
    sources/lcViewer.cpp

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(generated/qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()
