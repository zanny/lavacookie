/* lavacookie.hpp created 4/2/2013
 * Copyright (C) 2013 Matthew Scheirer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LAVACOOKIE_HPP
#define LAVACOOKIE_HPP

#include <string>
#include <iostream>
#include <vector>

#include <QDir>
#include <QHash>
#include <QIODevice>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonValueRef>

#include "lc.hpp"
#include "profile.hpp"

class LavaCookie {
public:
  static const LavaCookie &getInstance() const;
  void switchOrient();
private:
  LavaCookie() throw(PermissionsException);

  static const LavaCookie instance();

  /* After considerations of the complexity of implementing unordered_set hashes for both Profile
   * and QDir (QSet doesn't support QDir) I just went with vectors. So you have no guarantee you don't
   * have duplicates of the same profile / directory. If it becomes a problem, implement hashes and
   * replace these with sets */
  std::vector<Profile> profiles;
  std::vector<QDir> dataDirs;
  // QHash because it includes the hash() and key compare for QString
  QHash<QString, QDir&> dataFiles;

  QJsonDocument config;
  QJsonObject configObj;
  QJsonArray jsonProfiles;
};

#endif // LAVACOOKIE_HPP
