/* lavacookie.hpp created 3/28/2013
 * Copyright (C) 2013 Matthew Scheirer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Lavacookie global variable and function namespace.
#ifndef LC_HPP
#define LC_HPP

#include <exception>
#include <string>
#include <utility>
#include <vector>
#include <unordered_set>

#include <QDir>
#include <QFile>
#include <QHash>
#include <QString>
#include <QStringList>

#include "profile.hpp"

#define APPDIR "Doomsday"
#define ADDONDIR "Addons"
#define CFGNAME "lc.json"
// Since DKotDS is a pwad, we treat it as a special case (it is the only
// "addon" game that installs if detected)
#define HEXDD "hexdd.wad"

/* This exception is for internal use only - it will crash the app if it lacks
 * read and write permissions to ~/Doomsday, but since it is never user facing
 * doesn't require translation so it has less than pretty character constructs */
class PermissionsException : public std::exception {
public:
  explicit PermissionsException(std::string file) : badFile(file) {}
  virtual const char* what() const noexcept {
    return ("Insufficient privileges to create or modify " + badFile).c_str();
  }
private:
  std::string badFile;
};

namespace lc {
/* The application directory (~/Doomsday) the config file (~/Doomsday/lc.json) and the
 * primary addons directory (~/Doomsday/Addons) are all hard coded, but new addon directories
 * can be added to the config file via the app.  Game settings are managed by Doomsday, and
 * before a game is launched the working directory is set to baseDir */
const QDir baseDir(QDir::homePath() + '/' + APPDIR);
const QString basePath(baseDir.canonicalPath());
const QDir mainAddonDir(basePath + '/' + ADDONDIR);
const QFile configFile(QString(basePath + '/' + CFGNAME));
const QStringList dataFilters{QString("*.wad"), QString("*.box"),
                               QString("*.addon"), QString("*.pk3"),
                               QString("*.zip"), QString("*.deh"),
                               QString("*.ded"), QString("*.lmp")},
                  infoFilters{QString("info"), QString("info.conf")};
const std::string iwads[] {"doom.wad", "doom2.wad", "heretic.wad", "hexen.wad",
                           "plutonia.wad", "tnt.wad", "chex.wad", "hacx.wad"};


void addDataDir(QDir);
Profile genProfile(QJsonObject) throw(std::string);
void refreshAddons() throw(PermissionsException);

#endif // LC_HPP
