/* stable.hpp created 3/26/2013
 * Copyright (C) 2013 Matthew Scheirer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STABLE_HPP
#define STABLE_HPP

// These headers are precompiled by the .pro to make build times faster. I throw all the stl and qt
// includes here since I'm not modifying them.

#include <exception>
#include <string>
#include <utility>
#include <vector>

#include <QString>
#include <QObject>
#include <QResizeEvent>
#include <QDate>
#include <QDir>
#include <QFile>
#include <QHash>
#include <QIODevice>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValueRef>

#include <QGuiApplication>

#include "qtquick2applicationviewer.h"

#endif
