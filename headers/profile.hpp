/* profile.hpp created 3/28/2013
 * Copyright (C) 2013 Matthew Scheirer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROFILE_HPP
#define PROFILE_HPP

#include <utility>
#include <vector>

#include <QString>
#include <QDate>

#include "lavacookie.hpp"

struct Profile {
  QString name, description, game;
  std::vector<QString> addons, missingAddons;
  QDate lastPlayed;

  Profile(QString, QString, QString, QDate date = QDate());
};

#endif // PROFILE_HPP
