/* lcViewer.hpp created 4/3/2013
 * Copyright (C) 2013 Matthew Scheirer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LCVIEWER_HPP
#define LCVIEWER_HPP

#include <QObject>
#include <QResizeEvent>

#include "qtquick2applicationviewer.h"
#include "lavacookie.hpp"

// This class extends the included applicationviewer to override the resizeEvent.
// There are interfaces for a UI where width > height, and one for height > width.
class lcViewer : public QtQuick2ApplicationViewer {
  Q_OBJECT
};

#endif // LCVIEWER_HPP
