 I hate when projects include readmes without a file extension, mainly because I like static file typing.
 
 Lavacookie is a mobile-targeted launcher for the Doomsday addon to basically manage WADs, pk3s, and box's. I wrote it
 since Snowberry is written against wxWidgets, which doesn't support mobile platforms besides ios, requires an entirely
 new interface anyway, and since Doomsday proper is now using qt, QML seems like a logical choice, even if the ultimate
 coupling between the two is a command line call of the doomsday binary with the wads in the end. It also doesn't require
 a plugin interface like Snowberry, since it targets mobile devices - the use cases are different (see the manifesto).
 
 To build it, just run lavacookie.pro through qmake. Only requirement is qt 5.1 or "greater" (unless something new is
 not backwards compatible).
 
 Guidelines for how I wrote this (and anyone that might want to contribute, try to follow them as well):
 1) K&R style with 2 space indentation. 
 2) Namespaces aren't indented. 
 3) I have a max line length of 120 for readability more than anything.
 4) CamelCase names, unless all caps, in which case ALL_CAPS.
 5) Prioritize STL types over Qt ones, only for portability with any library I might use. If something in Qt wants
    Qt type, I'll just keep it in a Qt type (if a Qt function wants a QList, I'll just keep that list in a Qlist).
 6) I try to use descriptive variable names, but I don't want to see fooThingWithStupidlyLongName anywhere.
 7) If I break something across lines once (ie, an intializer list of an array) due to length, I'll consistently
    keep line breaking it for the rest of its declaration to the same indentation even if it doesn't overflow.
 8) I explicitly declare the private parts of my classes, but usually don't declare public in structs - bad habit,
   but I like having the public stuff followed by the private.
   
~ Matt Scheirer (matt.scheirer@gmail.com)